mod types;
use crate::types::*;
use ansi_term::Colour::{Blue,Red,White,Black}; /// to add color to terminal output


/// Will move a piece from a pos to another pos in the board.
extern fn move_piece (board: &Board, from: &Pos, to: &Pos) -> Board {
    match &board[usize::from(*from)] {
        Option::Some(p) => {
            let mut new_board = board.clone();
            new_board[usize::from(*from)] = None;
            new_board[usize::from(*to)] = Some(p.clone());
            return new_board 
        },
        Option::None => board.clone()
    }
}


/// Predicate: Position is valid for a 8x8 board. 
fn within_limit (pos: &Pos) -> bool {
    *pos >= 1 && *pos <= 64
}


/*-----------------*/
/* PRIMITIVE MOVES */
/*-----------------*/


fn up (pos: &Pos) -> Pos { pos + 8 }
fn dw (pos: &Pos) -> Pos { pos - 8 }
fn lf (pos: &Pos) -> Pos { pos - 1 }
fn rt (pos: &Pos) -> Pos { pos + 1 }

fn ul (pos: &Pos) -> Pos { pos + 7 }
fn ur (pos: &Pos) -> Pos { pos - 9 }
fn dl (pos: &Pos) -> Pos { pos - 7 }
fn dr (pos: &Pos) -> Pos { pos - 9 }


/*--------*/
/* TRACER */
/*--------*/


/// Will return all valid piece moves in a direction.
/// DIRECTION:u8 =: U, D, L, R, UL, UR, DL, DR
fn trace (max: u8, board: Board, dir: u8, start: &Pos) -> Vec<Pos> { 
    let mut acc: Vec<Pos> = Vec::new();
    let iter =
        match dir {
            1 => up, 2 => dw, 3 => lf, 4 => rt, 5 => ul, 6 => ur, 7 => dl, 8 => dr,
            _ => up // ?
        };
    let mut pos: Pos = *start;
    for _ in 1..max {
        let i = iter(&pos);
        if within_limit(&i) { 
            let refB = &board.clone();
            if !occupied(&board,&i) {
                if !in_check(&move_piece(refB, start, &i), &Player::White) { // within-notBlocked-notExposes
                    acc.push(iter(&pos));
                    pos = i;
                } else { pos = i } // within-notBlocked-exposes
            } else {
                if !in_check(&move_piece(refB, start, &i),&Player::White) { // within-blocked-notExposes
                    acc.push(iter(&pos));
                    break;
                } else { break }// within-blocked-exposes
            }
        } else { break }; // notwithin board 
    }
    return acc;
}


// ! this is screaming to be done with a polymorpihc filter
/// Return all pieces of a player in a board.
fn player_pieces (board: &Board, player: &Player) -> Vec<(Pos,Piece)> {
    let mut acc: Vec<(Pos,Piece)> = Vec::new();
    let mut pos: Pos = 0;
    for piece in board.iter() { 
        pos+=1;
        match piece {
            Some(p) => {
                if p.player == *player { 
                    acc.push((pos,p.clone())) 
                }
            }
            None => continue
        }
    }
    return acc;
}


// ! this is screaming to be done with a polymorpihc filter
/// Will return all occurrences of a chessman in board. Like all kings or all bishops.
fn get_chessman (board: &Board, chessman: &Chessman) -> Vec<(Pos,Piece)> {
    let mut acc: Vec<(Pos,Piece)> = vec![];
    let mut pos: Pos = 0;
    for piece in board.iter() { 
        pos+=1;
        match piece {
            None => continue,
            Some(p) => {
                if p.chessman == *chessman { 
                    acc.push((pos,p.clone())) 
                }
            }
        }
    }
    return acc;
}


/// Return king of a player from a board.
fn get_king (board: &Board, player: &Player) -> (Pos,Piece) {
    let kings = get_chessman(board,&Chessman::King);
    for king in kings { if king.1.player == *player { return king }}
    //return panic!{ "there is no king motherfucker" };
    return (200, Piece { player: Player::White, chessman: Chessman::Pawn })
}


/*------------*/
/* PREDICATES */
/*------------*/


/// Position is ocuppied.
fn occupied (board: &Board, pos: &Pos) -> bool {
    match board[usize::from(*pos)] {
        None => false,
        Some(_) => true
    }
}


/// Check if a player is in check.
fn in_check (board: &Board, player: &Player) -> bool {
    let king = get_king(&board,player);
    for mov in player_moves(board, &enemy(player)) {
        if king.0 == mov.to && mov.ty == MoveType::Capture { return true; }
    }
    return false;
}


/*-------------*/
/* AXIAL MOVES */
/*-------------*/


/// All parallel moves for a piece.
fn parallel (max: u8, board: &Board, start: &Pos) -> Vec<Pos> { 
    let mut acc: Vec<Pos> = Vec::new();
    for n in 1..5 {
        acc.append(&mut trace(max + 1, board.clone(), n, start))
    }
    return acc;
}


/// All diagonal moves for a piece.
fn diagonal (max: u8, board: &Board, start: &Pos) -> Vec<Pos> { 
    let mut acc: Vec<Pos> = Vec::new();
    for n in 5..9 {
        acc.append(&mut trace(max + 1, board.clone(), n, start))
    }
    return acc;
}


/*-------------*/
/* PIECE MOVES */
/*-------------*/


fn rook (board: &Board, start: &Pos) -> Vec<Pos> { 
    parallel(7,board,start)
}


fn bishop (board: &Board, start: &Pos) -> Vec<Pos> { 
    diagonal(7,board,start) 
}


fn king  (board: &Board, start: &Pos) -> Vec<Pos> { 
    let mut foo = parallel(1,board,start);
    let mut bar = diagonal(1,board,start);
    foo.append(&mut bar); 
    return foo;
}


fn queen (board: &Board, start: &Pos) -> Vec<Pos> { 
    let mut foo = parallel(7,board,start);
    let mut bar = diagonal(7,board,start);
    foo.append(&mut bar); 
    return foo;
}


fn pawn (board: &Board, start: &Pos) -> Vec<Pos> { 
    parallel(7,board,start) 
} // TODO
// ! use parallel and stuff and them filter them


fn knight (board: &Board, start: &Pos) -> Vec<Pos> { 
    parallel(7,board,start)
} // todo
// ! use parallel and stuff and them filter them


/*-------*/
/* MOVES */
/*-------*/


// classify
// if player piece in enemy row, become type
// if above other piece, capturing
// otherwise regular
fn classify (board: Board, from: &Pos, to: Pos) -> Move { 
    let capturing = occupied(&board, &to);
    match &board[usize::from(*from)] {
        Option::Some(piece) => {
            let becom = 
                if to <= 8 && piece.player == Player::Black {
                    true
                } else if to >= 57 && piece.player == Player::White {
                    true
                } else {
                    false
                };

            return Move { 
                from: *from,
                to: to,
                ty: 
                    if capturing { 
                        if becom {
                            MoveType::CaptureBecome
                        } else {
                            MoveType::Capture
                        }
                    } else { 
                        if becom {
                            MoveType::RegularBecome
                        } else {
                            MoveType::Regular
                        }
                    }
            }
        },
        Option::None => unreachable!()
    }
}


// Return all valid moves for a piece.
fn piece_moves (board: &Board, from: &Pos) -> Option<Vec<Move>> {
    let mut acc: Vec<Move> = Vec::new();
    let foo =
        match &board[usize::from(*from)] {
            None => None,
            Some(piece) =>
                match piece.chessman {
                    Chessman::Pawn   => Some(pawn(&board,from)),
                    Chessman::Rook   => Some(rook(&board,from)),
                    Chessman::Knight => Some(knight(&board,from)),
                    Chessman::Bishop => Some(bishop(&board,from)),
                    Chessman::King   => Some(king(&board,from)),
                    Chessman::Queen  => Some(queen(&board,from))
                }
        };
    let tos = match foo {
        Some(t) => t,
        _       => Vec::new()
    };
    for to in tos {
        acc.push(classify(board.clone(), from, to))
    }
    return Some(acc);
}


// Return all valid moves for a player.
fn player_moves (board: &Board, player: &Player) -> Vec<Move> {
    let mut acc: Vec<Move> = Vec::new();
    for (pos,piece) in player_pieces(&board, player) {
        match &mut piece_moves(board,&pos) {
            Some(p) => acc.append(p),
            _       => panic!{}
        }
    }
    return acc;
}


/*-----------*/
/* UTILITIES */
/*-----------*/


// Reverse player color.
fn enemy (player: &Player) -> Player {
    match player {
        Player::White => Player::Black,
        Player::Black => Player::White
    }
}


/*--------*/
/* RENDER */
/*--------*/


// this looks awful and is gigantic
fn render(board: &Board) -> () {
    let mut counter: u8 = 1;
    let mut sqr_color: Player = Player::White;
    for pos in board.iter() {
        let pos_color = match sqr_color {
            Player::White => White,
            Player::Black => Black
        };
        match pos {
            None => {
                print!("{}", Red.on(pos_color).paint(" "));
            }
            Some(p) => {
                let chessman = match p.chessman {
                    Chessman::Pawn   => "♟",
                    Chessman::Rook   => "♜",
                    Chessman::King   => "♚",
                    Chessman::Bishop => "♝",
                    Chessman::Knight => "♞",
                    Chessman::Queen  => "♛"
                };
                let player_c = match p.player {
                    Player::White => Red,
                    Player::Black => Blue
                };
                print!("{}", player_c.on(pos_color).paint(chessman));
            }
        }        
        if counter == 8 {
            println!("");
            counter = 1;
        } else {
            counter += 1;
            sqr_color = enemy(&sqr_color);
        }
    }
}


/*-----------*/
/* UTILITIES */
/*-----------*/


static INIT_BOARD : Board = [
    Some(Piece { player: Player::White, chessman: Chessman::Rook }),
    Some(Piece { player: Player::White, chessman: Chessman::Knight }),
    Some(Piece { player: Player::White, chessman: Chessman::Bishop }),
    Some(Piece { player: Player::White, chessman: Chessman::Queen }),
    Some(Piece { player: Player::White, chessman: Chessman::King }),
    Some(Piece { player: Player::White, chessman: Chessman::Bishop }),
    Some(Piece { player: Player::White, chessman: Chessman::Knight }),
    Some(Piece { player: Player::White, chessman: Chessman::Rook }),

    Some(Piece { player: Player::White, chessman: Chessman::Pawn }),
    Some(Piece { player: Player::White, chessman: Chessman::Pawn }),
    Some(Piece { player: Player::White, chessman: Chessman::Pawn }),
    Some(Piece { player: Player::White, chessman: Chessman::Pawn }),
    Some(Piece { player: Player::White, chessman: Chessman::Pawn }),
    Some(Piece { player: Player::White, chessman: Chessman::Pawn }),
    Some(Piece { player: Player::White, chessman: Chessman::Pawn }),
    Some(Piece { player: Player::White, chessman: Chessman::Pawn }),

    None,None,None,None,None,None,None,None,
    None,None,None,None,None,None,None,None,
    None,None,None,None,None,None,None,None,
    None,None,None,None,None,None,None,None,

    Some(Piece { player: Player::Black, chessman: Chessman::Pawn }),
    Some(Piece { player: Player::Black, chessman: Chessman::Pawn }),
    Some(Piece { player: Player::Black, chessman: Chessman::Pawn }),
    Some(Piece { player: Player::Black, chessman: Chessman::Pawn }),
    Some(Piece { player: Player::Black, chessman: Chessman::Pawn }),
    Some(Piece { player: Player::Black, chessman: Chessman::Pawn }),
    Some(Piece { player: Player::Black, chessman: Chessman::Pawn }),
    Some(Piece { player: Player::Black, chessman: Chessman::Pawn }),

    Some(Piece { player: Player::Black, chessman: Chessman::Rook }),
    Some(Piece { player: Player::Black, chessman: Chessman::Knight }),
    Some(Piece { player: Player::Black, chessman: Chessman::Bishop }),
    Some(Piece { player: Player::Black, chessman: Chessman::Queen }),
    Some(Piece { player: Player::Black, chessman: Chessman::King }),
    Some(Piece { player: Player::Black, chessman: Chessman::Bishop }),
    Some(Piece { player: Player::Black, chessman: Chessman::Knight }),
    Some(Piece { player: Player::Black, chessman: Chessman::Rook }),
];


pub fn main() { render(&INIT_BOARD) }