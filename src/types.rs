/// Player color.
#[derive(PartialEq)]
#[derive(Eq)]
#[derive(Clone)]
pub enum Player { White, Black }


/// Chessman.
#[derive(PartialEq)]
#[derive(Eq)]
#[derive(Clone)]
pub enum Chessman { Pawn, Rook, Knight, Bishop, King, Queen }


/// Position in the board.
pub type Pos = u8;


/// Type of a move made.
#[derive(PartialEq)]
#[derive(Eq)]
pub enum MoveType { Capture, Regular, RegularBecome, CaptureBecome }


/// A move.
pub struct Move { pub from: Pos, pub to: Pos, pub ty: MoveType}


/// A piece representation.
#[derive(PartialEq)]
#[derive(Eq)]
#[derive(Clone)]
pub struct Piece { pub player: Player, pub chessman: Chessman }


/// Sort of bit board.
pub type Board = [Option<Piece>;64];